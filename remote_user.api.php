<?php

/**
 * @file
 * API functions.
 */

/**
 * Provide UserProvider implementations for the user to choose from.
 *
 * @return array
 *   Returns an associative array keyed with the provider name containing
 *   arrays with the following keys:
 *   - title: The human readable name for this provider.
 *   - description: (Optional) A human readable description.
 *   - class: The name of the provider class.
 */
function hook_remote_user_provider_info() {
  return array(
    'my_plugin' => array(
      'title' => t('My remote user provider'),
      'description' => t('A user provider that is all mine!'),
      'class' => 'MyRemoteUserProvider',
    ),
  );
}

/**
 * Alter the new local user before it is created.
 *
 * @param array $userinfo
 *   Array representing user that will be passed to user_save().
 * @param object $remote
 *   An object representing the remote user.
 * @param RemoteUserProvider $provider
 *   (Optional) A remote user provider object.
 */
function hook_remote_user_create_local_account_alter(&$userinfo, $remote, $provider) {
  if ($provider->getProviderName() == 'my_plugin') {
    // Set some other field.
    $userinfo['field_other']['und'][0]['value'] = $remote->other;
  }
}

/**
 * Alter the local user after provider but before it is saved.
 *
 * @param object $account
 *   A Drupal account object.
 * @param object $remote
 *   An object representing the remote user.
 * @param RemoteUserProvider $provider
 *   A remote user provider object.
 */
function hook_remote_user_update_local_account_alter(&$account, $remote, $provider) {
  if ($provider->getProviderName() == 'my_plugin') {
    // Set some other field.
    $account['field_other']['und'][0]['value'] = $remote->other;
  }
}
