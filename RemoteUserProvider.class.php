<?php
/**
 * @file
 * Contains the RemoteUserProvider interface.
 */

/**
 * Interface for remote user providers.
 */
abstract class RemoteUserProvider {
  protected $info;

  /**
   * Construct a new RemoteUserProvider.
   */
  public function __construct($info) {
    $this->info = $info;
  }

  /**
   * Get the provider name.
   */
  public function getProviderName() {
    return $this->info['name'];
  }

  /**
   * Get the provider module.
   */
  public function getProviderModule() {
    return $this->info['module'];
  }

  /**
   * Check if the provider is ready or if additional configuration is needed.
   *
   * @return boolean
   *   Returns TRUE if ready; otherwise FALSE.
   */
  public abstract function isReady();

  /**
   * Authenticate the user against the remote provider.
   *
   * @param array $form
   *   The user login form.
   * @param array &$form_state
   *   The state from the user login form. You probably want to look at
   *   $form_state['values']['name'] and $form_state['values']['pass'].
   * 
   * @return object
   *   An object representing the remote user; or NULL if authentication failed.
   */
  public abstract function authenticate($form, &$form_state);

  /**
   * Alter the user login form.
   *
   * @param array &$form
   *   The user login form.
   * @param array &$form_state
   *   The user login form state.
   * @param string $form_id
   *   The ID of the login form. Could be 'user_login' or 'user_login_block'.
   */
  public function alterLoginForm(&$form, &$form_state, $form_id) { }

  /**
   * Alter the user account form.
   *
   * @param array &$form
   *   The user login form.
   * @param array &$form_state
   *   The user login form state.
   * @param string $form_id
   *   The ID of the login form. Could be 'user_profile_form' or
   *   'user_register_form'.
   */
  public function alterUserAccountForm(&$form, &$form_state, $form_id) { }

  /**
   * A validation callback for 'user_profile_form' or 'user_register_form'.
   *
   * This is where you should verify that the 'name' and 'mail' aren't already
   * taken on the remote server. Call form_set_error() if a problem is found.
   *
   * @param array &$form
   *   The user login form.
   * @param array &$form_state
   *   The user login form state.
   *
   * @see form_set_error()
   */
  public function validateUserAccountForm(&$form, &$form_state) { }

  /**
   * Alter the 'user_pass' form.
   *
   * @param array &$form
   *   The user password form.
   * @param array &$form_state
   *   The user password form state.
   */
  public function alterUserPasswordForm(&$form, &$form_state) { }

  /**
   * A validation callback for 'user_pass' form.
   *
   * This is where you should verify that the e-mail or username are valid and
   * allowed by the remote server. Call form_set_error() if a problem is found.
   *
   * @param array &$form
   *   The user password form.
   * @param array &$form_state
   *   The state from the user password form. You probably want to look at
   *   $form_state['values']['name'] or $form_state['values']['account'] (which
   *   contains the user account object for the given user by 'name').
   *
   * @see form_set_error()
   */
  public function validateUserPasswordForm(&$form, &$form_state) { }

  /**
   * Get a unique ID from the remote user object.
   *
   * @param object $remote
   *   An object representing the remote user.
   *
   * @return string
   *   Unique identifier string from the remote user data.
   */
  public abstract function getRemoteId($remote);
  
  /**
   * Get the remote user object from its ID.
   *
   * @param string $remote_id
   *   Unique string identifying the remote user.
   *
   * @return object
   *   An object representing the remote user; or NULL if not found.
   */
  public abstract function getRemoteUser($remote_id);

  /**
   * Creates a remote user.
   *
   * @param object $account
   *   A Drupal user object.
   * @param array $edit
   *   An array of values from the user create form. Use $edit['pass_plain']
   *   to get the password in plain text.
   *
   * @return object
   *   An object representing the remote user; or NULL if failed.
   */
  public abstract function createRemoteUser($account, $edit);
  
  /**
   * Update a remote user with the Drupal user's data.
   *
   * @param object $account
   *   A Drupal user object.
   * @param array $edit
   *   An array of values from the user create form. Use $edit['pass_plain']
   *   to get the password in plain text.
   *
   * @return bool
   *   TRUE if successful; or FALSE otherwise.
   */
  public abstract function updateRemoteUser($account, $edit);

  /**
   * Delete a remote user.
   *
   * @param string $remote_id
   *   Unique string identifying the remote user.
   *
   * @return bool
   *   TRUE if successful; or FALSE otherwise.
   */
  public abstract function deleteRemoteUser($remote_id);

  /**
   * Update a local user with information from the remote user.
   *
   * This function is responsible for calling user_save() if
   * necessary.
   *
   * @param object $account
   *   A Drupal user object.
   * @param object $remote
   *   An object representing the remote user.
   *
   * @return bool
   *   TRUE if successful; or FALSE otherwise.
   */
  public abstract function updateLocalUser($account, $remote);
}
