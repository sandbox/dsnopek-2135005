<?php
/**
 * @file
 * Contains RemoteUserTestSuccessProvider.
 */

/**
 * A RemoteUserProvider that returns positive responses to everything.
 */
class RemoteUserTestSuccessProvider extends RemoteUserProvider {
  /**
   * {@inheritdoc}
   */
  public function isReady() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate($form, &$form_state) {
    drupal_set_message(t('Success with test!'));
    return (object)array('id' => $form_state['values']['name']);
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteId($remote) {
    return (string)$remote->id;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getRemoteUser($remote_id) {
    return (object)array('id' => $remote_id);
  }

  /**
   * {@inheritdoc}
   */
  public function createRemoteUser($account, $edit) {
    return (object)array('id' => $account->name);
  }
  
  /**
   * {@inheritdoc}
   */
  public function updateRemoteUser($account, $edit) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRemoteUser($remote_id) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateLocalUser($account, $remote) {
    return TRUE;
  }
}
