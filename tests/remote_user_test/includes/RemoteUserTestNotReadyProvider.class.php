<?php
/**
 * @file
 * Contains RemoteUserTestNotReadyProvider.
 */

/**
 * A RemoteUserProvider that is never ready (but otherwise always succeeds).
 */
class RemoteUserTestNotReadyProvider extends RemoteUserTestSuccessProvider {
  /**
   * {@inheritdoc}
   */
  public function isReady() {
    return FALSE;
  }
}
