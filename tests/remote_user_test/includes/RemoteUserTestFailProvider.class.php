<?php
/**
 * @file
 * Contains RemoteUserTestFailProvider.
 */

/**
 * A RemoteUserProvider that fails for all operations - except isReady().
 */
class RemoteUserTestFailProvider extends RemoteUserTestSuccessProvider {
  /**
   * {@inheritdoc}
   */
  public function authenticate($form, &$form_state) {
    drupal_set_message(t('Failure with test!'));
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteUser($remote_id) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createRemoteUser($account, $edit) {
    return NULL;
  }
  
  /**
   * {@inheritdoc}
   */
  public function updateRemoteUser($account) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRemoteUser($remote_id) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function updateLocalUser($account, $remote) {
    return FALSE;
  }
}
