<?php

/**
 * @file
 * Admin pages for Remote User API.
 */

/**
 * Form callback for settings page.
 */
function remote_user_settings_form() {
  $form = array();

  $providers = remote_user_providers();

  // TODO: Do something with the 'description'!
  $options = array('' => t('<em>None.</em> Authenticate users against the local system.'));
  foreach ($providers as $name => $info) {
    $options[$name] = $info['title'];
  }

  $form['remote_user_active_provider_name'] = array(
    '#title' => t('Remote user provider'),
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => remote_user_active_provider_name(),
  );

  return system_settings_form($form);
}
